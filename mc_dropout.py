#!/usr/bin/env python3

import random
import time
from pathlib import Path
from typing import Union, Optional

import numpy as np
import pandas as pd
import torch
# noinspection PyPackageRequirements
from mytorch.utils.goodies import FancyDict
from tqdm.auto import trange

from run import Runner
import argparse


def enable_dropout(model: torch.nn.Module):
    """ Function to enable the dropout layers during test-time """
    for m in model.modules():
        if m.__class__.__name__.startswith('Dropout'):
            m.train()


def get_vanilla_pred(model: Runner,
                     sub: Union[str, int],
                     rel: Union[str, int]
                     ):
    if type(sub) != type(rel):
        raise TypeError(f"Both subject (type: {type(sub)}) and relation (type: {type(rel)})"
                        " should be of the same type")

    if type(sub) == str:
        sub = model.ent2id[sub]
        sub = [sub, sub]
        rel = model.rel2id[rel]
        rel = [rel, rel]
    elif type(sub) == int:
        sub = [sub, sub]
        rel = [rel, rel]

    # Now we're ready
    t_sub = torch.tensor(sub, dtype=torch.int32, device=model.device)
    t_rel = torch.tensor(rel, dtype=torch.int32, device=model.device)
    model.model.eval()
    with torch.no_grad():
        op = model.model.forward(t_sub, t_rel)[0].detach().cpu()
    return op


# For simplicity's sake we write a cold for loop (no batching) to get results
# noinspection PyTypeChecker
def get_predictions(model: Runner,
                    sub: Union[str, int, list],
                    rel: Union[str, int, list],
                    #                     objs: List[List[Union[int, str]]] = None,
                    n: int = 50):
    """
        **sub**, and **rel** can be a str, an int, a list of str, or a list of int:
            'soleil i.a'
            1
            ['soleil i.a', 'lune i.a']
            [1, 2]

        If a list, we treat this as a batch.
        If a single value, we make it into a batch of 2

        ~~**objs** must be a list (but can contain strings or ints)~~
            ~~If not provided, we find the based on get_gold~~

        Recommended to not change n during one run. Bad things will happen.
    """
    if type(sub) != type(rel):
        raise TypeError(f"Both subject (type: {type(sub)}) and relation (type: {type(rel)})"
                        " should be of the same type")

    if type(sub) == str:
        sub = model.ent2id[sub]
        # sub = [sub, sub]
        rel = model.rel2id[rel]
        # rel = [rel, rel]
        singular = True
    elif type(sub) == int:
        # sub = [sub, sub]
        # rel = [rel, rel]
        singular = True
    # elif type(sub) == list:
    #     if len(sub) != len(rel):
    #         raise ValueError(f"Both subject (len: {len(sub)}) and relation (len: {len(rel)})"
    #                          " should be of the same length")
    #     for i in range(len(sub)):
    #         sub[i] = sub[i] if type(sub[i]) is int else model.ent2id[sub[i]]
    #         rel[i] = rel[i] if type(rel[i]) is int else model.rel2id[rel[i]]
    #     singular = False
    else:
        raise TypeError(f'Unknown type for sub: {type(sub)}.')

    # Now we're ready
    t_sub = torch.tensor(sub, dtype=torch.int32, device=model.device).unsqueeze(-1)
    t_rel = torch.tensor(rel, dtype=torch.int32, device=model.device).unsqueeze(-1)

    # if singular:
    pred = torch.zeros(n, model.p.num_ent, dtype=torch.float32, device=model.device)
    # else:
    #     pred = torch.zeros(len(sub), n, model.p.num_ent, dtype=torch.float32, device=model.device)
    model.model.eval()
    enable_dropout(model.model)

    with torch.no_grad():
        for i in trange(n, desc="Actual Prediction", leave=False):
            pred[i] = model.model.forward(t_sub, t_rel)

    return pred



# noinspection PyTypeChecker
def run(
        dataset: str = 'RezoJDM16k',
        checkpoint: str = './checkpoints/jdm-dropout-1809',
        n_samples: int = 100,
        save_every: int = 100,
        gpu: str = '0',
        start_file = -1,
        count_file = -1,

):
    args = {'name': 'testrun',
            'dataset': dataset,
            'model': 'compgcn',
            'score_func': 'conve',
            'opn': 'corr',
            'use_wandb': False,
            'batch_size': 128,
            'gamma': 40.0,
            'gpu': gpu,
            'max_epochs': 1,
            'l2': 0.0,
            'lr': 0.001,
            'lbl_smooth': 0.1,
            'num_workers': 10,
            'seed': 41504,
            'restore': False,
            'bias': False,
            'num_bases': -1,
            'init_dim': 100,
            'gcn_dim': 200,
            'embed_dim': None,
            'gcn_layer': 1,
            'dropout': 0.05,
            'hid_drop': 0.15,
            'hid_drop2': 0.15,
            'feat_drop': 0.15,
            'k_w': 10,
            'k_h': 20,
            'num_filt': 200,
            'ker_sz': 7,
            'log_dir': './log/',
            'config_dir': './config/',
            'trim': False,
            'trim_ratio': 0.00005,
            'use_fasttext': False
            }
    args = FancyDict(args)

    model = Runner(args)
    # # Now load the saved model
    model.load_model(checkpoint)

    torch.manual_seed(42)
    np.random.seed(42)
    random.seed(42)

    # Step 1: Try to load dataframes for this dataset
    pathdir = Path('./mc_dropout') / Path(checkpoint).name
    try:
        df = pd.read_pickle(pathdir / 'graph.pickle')
    except FileNotFoundError:
        # Create the folder in case that doesn't exist either
        pathdir.mkdir(parents=True, exist_ok=True)

        df_tr = pd.DataFrame(model.data['train'], columns=['sub', 'rel', 'obj'])
        df_vl = pd.DataFrame(model.data['valid'], columns=['sub', 'rel', 'obj'])
        df_ts = pd.DataFrame(model.data['test'], columns=['sub', 'rel', 'obj'])

        tr_groups = df_tr.groupby(['sub', 'rel'])['obj'].apply(set)
        tr_groups = tr_groups.reset_index(name='train_objs')
        vl_groups = df_vl.groupby(['sub', 'rel'])['obj'].apply(set)
        vl_groups = vl_groups.reset_index(name='valid_objs')
        ts_groups = df_ts.groupby(['sub', 'rel'])['obj'].apply(set)
        ts_groups = ts_groups.reset_index(name='test_objs')

        def row_union(x: pd.Series) -> list:
            res = \
                set().union(x['train_objs'] if x['train_objs'] != -1 else set()
                            ).union(x['valid_objs'] if x['valid_objs'] != -1 else set()
                                    ).union(x['test_objs'] if x['test_objs'] != -1 else set())
            return list(res)

        df = pd.merge(left=tr_groups, right=vl_groups, how='outer', on=['sub', 'rel'])
        df = pd.merge(left=df, right=ts_groups, how='outer', on=['sub', 'rel'])
        df = df.fillna(-1)
        df['all_obj'] = df.apply(row_union, axis=1)
        df['index'] = df.index

        # Save this to disk
        df.to_pickle(pathdir / 'graph.pickle')

    # Now, we batch every {save_every} sub, rel combinations.
    # We process one sub, rel at once and don't use batches.
    # Why? Because this will be the most random way to do it

    #First find how many batches have already been generated
    if start_file == -1:
        try:
            start_batch = max(int(filename.name.split('.')[0]) for filename in pathdir.rglob('*.torch'))
        except ValueError:
            start_batch = 0
            print('Found no stored files. Will start from the start')
    else:
        start_batch = start_file * save_every

    if count_file == -1:
        end_batch = df.shape[0]
    else:
        end_batch = start_batch + (count_file * save_every)
        if (end_batch > df.shape[0]):
            end_batch = df.shape[0]

    for batch_i in trange(start_batch, end_batch, save_every, desc=f"Main loop"):
        # Save {batchsize} predictions (no dropout; default way)
        # Save {batchsize}*{saveevery} MC Dropout predictions
        mcd_preds: Optional[torch.Tensor] = None  # We use this var to keep appending predictions to.
        vanilla_preds = []

        for i in trange(batch_i, batch_i + save_every, desc=f"File \"{batch_i+save_every}.torch\": from {batch_i} to {batch_i+save_every-1}", leave=False):
            try:
                sub, rel = df['sub'][i], df['rel'][i]
            except (KeyError, IndexError) as e:
                # we must be over the df limit
                break

            mcd_pred = get_predictions(model, int(sub), int(rel), n=n_samples)  # (n, num_ent)
            vanilla_preds.append(get_vanilla_pred(model, int(sub), int(rel)))
            if mcd_preds is not None:
                mcd_preds = torch.cat((mcd_preds, mcd_pred.unsqueeze(0)))  # (i, n, num_ent)
                del mcd_pred
            else:
                mcd_preds = mcd_pred.unsqueeze(0)

        # Dump this to disk
        torch.save({
            "mc_dropout": mcd_preds,
            "vanilla": torch.vstack(vanilla_preds)
        }, pathdir / f"{batch_i + save_every}.torch")
        del mcd_preds, vanilla_preds


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parser For Arguments', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-start-file', dest='start_file', type=int, default=-1, help='Start from a specific torch file')
    parser.add_argument('-count-file', dest='count_file', type=int, default=-1, help='Number of files to compute')
    parser.add_argument('-dataset', dest='dataset', type=str, default='RezoJDM16k', help='Choose the dataset')
    parser.add_argument('-checkpoint', dest='checkpoint', type=str, default='jdm-dropout-1809', help='Checkpoint of the trained model')
    parser.add_argument('-save-every', dest='save_every', type=int, default=100, help='Generate torch file every x')

    args = parser.parse_args()

    run(dataset=args.dataset, checkpoint=args.checkpoint, save_every=args.save_every, start_file=args.start_file, count_file=args.count_file)
