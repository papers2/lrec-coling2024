#!/usr/bin/env python3

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import collections
import dataclasses
from typing import Set, Any
import xml.etree.ElementTree as ET
import argparse
from pathlib import Path
from random import randint

@dataclasses.dataclass
class Triplets:
    train: Any
    valid: Any
    test: Any

@dataclasses.dataclass
class Nodes:
    train: Set = dataclasses.field(default_factory=set)
    valid: Set = dataclasses.field(default_factory=set)
    test: Set = dataclasses.field(default_factory=set)
    intersection : Set = dataclasses.field(default_factory=set)

def get_entities(df_nodes):
    """
        Get label of each node and new ids
    """
    entities = df_nodes['lexname'].tolist()
    list_labels = []

    # Get the label and features between tags in xml format
    for ent in entities:
        tag = ET.fromstring('<tag>'+ent+"</tag>")
        acc = ""
        for field in tag:
            acc = acc + " " + field.text
        list_labels.append(acc.strip())

    df_nodes['label'] = list_labels

    # if export:
    #     df_nodes['label'].to_csv('data/RLF/entities.txt', index=True, sep='\t', header=False)

    return df_nodes

def get_lf_families():
    """
    Returns dictionary with lf family and its corresponding lf
    """

    dic = {}
    tree = ET.parse("data/RLF/originals/14-lslf-model.xml")
    root = tree.getroot()
    for child in root:
        for gc in child:
            dic[gc.attrib['id']] = [ggc.attrib['id'] for ggc in gc]
    return dic

def get_default_format(df, entities, relations):
    """
        Put labels for relations and entities
    """
    ent_map = dict(zip(entities['id'].tolist(), entities['label'].tolist()))
    rel_map = dict(zip(relations['id'].tolist(), relations['name'].tolist()))

    df['type'] = df['type'].map(rel_map)
    df['source'] = df['source'].map(ent_map)
    df['target'] = df['target'].map(ent_map)

    #df = df.drop(columns='id')

    return df

def filter_set(df, train):
    train_ent = set(train['source'].tolist() + train['target'].tolist())
    train_rel = set(train['type'].tolist())

    df = df[df['source'].isin(train_ent)]
    df = df[df['target'].isin(train_ent)]
    df = df[df['type'].isin(train_rel)]

    return df

def split(df_triplets, status):
    """
        Divide triplets into train, valid, test sets (80%, 10%, 10%)
        #Return triplets and nodes in each set
        Status for random state
    """
    train, valid, test = np.split(df_triplets.sample(frac=1, random_state=status), [int(.8*len(df_triplets)), int(.9*len(df_triplets))])
    valid, test = filter_set(valid, train), filter_set(test, train)
    triplets = Triplets(train, valid, test)

    # nodes = Nodes()
    # nodes.train = set(train["source"]).union(set(train["target"]))
    # nodes.valid = set(valid["source"]).union(set(valid["target"]))
    # nodes.test = set(test["source"]).union(set(test["target"]))
    # nodes.intersection =  nodes.train.intersection(nodes.valid).intersection(nodes.test)

    return triplets

def get_triplets():

    #nodes
    nodes = pd.read_csv("data/RLF/originals/01-lsnodes.csv", sep="\t")
    entities = get_entities(nodes)

    # relations
    lf = pd.read_csv("data/RLF/originals/lf-ids-names.csv", sep="\t")
    lffam = pd.read_csv("data/RLF/originals/lffam-ids-names.csv", sep="\t")
    cp = pd.read_csv("data/RLF/originals/cp-ids-names.csv", sep="\t")

    # triplets
    df_lf = pd.read_csv("data/RLF/originals/15-lslf-rel.csv", sep="\t")
    df_lf = df_lf[['source', 'lf', 'target']]
    df_lf.columns = df_lf.columns.str.replace("lf", "type")

    df_cp = pd.read_csv("data/RLF/originals/04-lscopolysemy-rel.csv", sep="\t")
    df_cp = df_cp[['source', 'type', 'target']]

    if args.dataset == 'lf':
        triplets = df_lf
        relations = lf

    elif args.dataset == 'lf-cp':
        triplets = pd.concat([df_lf, df_cp], ignore_index=True)
        relations = pd.concat([lf, cp], ignore_index=True)

    elif args.dataset == 'lffam':
        lf_fam = get_lf_families()
        triplets = df_lf.copy()
        for fam, lf in lf_fam.items():
            triplets['type'] = triplets['type'].replace(lf, fam)

        relations = lffam

    elif args.dataset == 'lffam-cp':
        lf_fam = get_lf_families()
        df_lffam = df_lf.copy()
        for fam, lf in lf_fam.items():
            df_lffam['type'] = df_lffam['type'].replace(lf, fam)

        triplets = pd.concat([df_lffam, df_cp], ignore_index=True)
        relations = pd.concat([lffam, cp], ignore_index=True)

    pathdir = Path('./data/RLF') / Path(args.dataset).name
    pathdir.mkdir(parents=True, exist_ok=True)

    entities['label'].to_csv(pathdir / 'entities.txt', index=True, sep="\t", header=False)
    relations['name'].to_csv(pathdir / 'relations.txt', index=True, sep="\t", header=False)

    triplets = get_default_format(triplets, entities, relations)
    triplets.to_csv(pathdir / 'triplets.txt', index=False, sep="\t")

    data = split(triplets, args.seed)
    data.train.to_csv(pathdir / 'train.txt', index=False, sep="\t", header=False)
    data.valid.to_csv(pathdir / 'valid.txt', index=False, sep="\t", header=False)
    data.test.to_csv(pathdir / 'test.txt', index=False, sep="\t", header=False)

    filter_triplets = pd.concat([data.train, data.valid, data.test])
    filter_triplets.to_csv(pathdir / 'filter_triplets.txt', index=False, sep="\t", header=False)


    return triplets


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parser For Arguments', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-data', dest='dataset', default='lf', help='Dataset to use, default: lf')
    parser.add_argument('-seed', dest='seed', default=randint(0, 10000000), type=int, help='Dataset to use, default: lf')

    args = parser.parse_args()

    # triplets
    triplets = get_triplets()
