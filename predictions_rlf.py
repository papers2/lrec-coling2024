#!/usr/bin/env python3

import pandas as pd
import numpy as np
import torch
from tqdm.auto import tqdm, trange
from matplotlib import pyplot as plt
from pathlib import Path
from tabulate import tabulate
from run import Runner
from collections import Counter
import networkx as nx
from mytorch.utils.goodies import FancyDict


def get_graph_nx(graph):
    # graph in networkx object MultiDiGraph = Directed graph with multiple edges between two nodes
    graph.columns = ["source", "type", "target"]
    graph[graph.columns] = graph.apply(lambda x: x.str.strip())
    graph['source'] = graph['source'].str.lower()
    graph['target'] = graph['target'].str.lower()
    graph['type'] = graph['type'].str.lower()

    Graphtype = nx.MultiDiGraph()
    graph_nx = nx.from_pandas_edgelist(graph, edge_attr='type', create_using=Graphtype)

    return graph_nx


def get_degree(sub: str) -> int:
    global nodes_degree
    nodes_degree_dict = dict(nodes_degree)
    return nodes_degree_dict[sub]


def _get_path_(sub: str, obj: str):
    global graph_nx

    # Try to get sp
    try:
        sp = nx.shortest_path(graph_nx, sub, obj)
    except nx.NetworkXNoPath:
        return []

    pathGraph = nx.path_graph(sp)

    real_path = []
    for ea in pathGraph.edges():
        edge = graph_nx.get_edge_data(ea[0], ea[1])
        if len(edge) == 2:
            # raise IOError(f"Found multiple edges. Copy paste all of this to Priyansh stp")
            continue
        val = list(edge.values())[0]['type']
        real_path.append(val)
    return real_path


def get_path(sub: str, obj: str):
    right_rel = _get_path_(sub=sub, obj=obj)
    opposite_rel = _get_path_(sub=obj, obj=sub)
    return right_rel, opposite_rel


# Functions which calculate the actual predictions
def top_k(preds, k=10):
    # Get the top k candidates for each prediction and make a mask like that (boolean)
    top_k = preds.argsort(dim=1, descending=True)[:, :k]
    res = torch.zeros_like(preds, dtype=torch.bool)
    for i, candidates in enumerate(top_k):
        res[i, candidates] = True

    return res


def score_threshold(preds, threshold=0.1):
    return preds > threshold


def avg(pred_masked):
    per_entity_freq = pred_masked.mean(dim=0)
    # print top ten candidates
    scores, indices = per_entity_freq.sort(descending=True)[:10]
    print("Top scoring candidates by this method: \n\n(conf): node")
    for i in range(10):
        print(f"{scores[i]:.10f}: {indices[i].item()}")

    return per_entity_freq


def aggregate(raw, selected, model: Runner, row: pd.Series, df_ind: int,
              conf_threshold=0.5, avg_threshold=0, max_items: int = 10, ):
    # Selected is a boolean mat of (num_ent, num_samples)
    selected = selected.float()
    per_entity_freq = selected.mean(dim=0)
    avg = raw.mean(dim=0)

    # print top ten candidates
    conf, indices = per_entity_freq.sort(descending=True)
    conf = conf[:max_items + len(row['all_obj'])]
    indices = indices[:max_items + len(row['all_obj'])]
    # print("Top scoring candidates by this method: \n\n(conf): (agg. score): node")
    # print("Top scoring candidates by this method: \n\n")
    headers = ['Sub ID', 'Rel ID', 'Obj ID', 'Sub Nm', 'Rel Nm', 'Obj Nm', 'Conf', 'Score', 'Sub Deg', 'Obj Deg',
               'Short. Dist.', 'Right Path', 'Left Path']
    table_rows = []
    for conf, obj_id in zip(conf, indices):

        # Filter out the ones which are in original graph
        if obj_id in row['all_obj']:
            continue

        # Filter out the ones which have low confidence
        if conf < conf_threshold:
            continue

        avg_score = round(avg[obj_id].item(), 8)
        if avg_score < avg_threshold:
            continue

        # confidence score is _conf
        # object ID is _obj_id

        sub_id = row['sub'].item()
        rel_id = row['rel'].item()
        obj_id = obj_id.item()

        # Names
        sub_name = model.id2ent[sub_id]
        rel_name = model.id2rel[rel_id]
        obj_name = model.id2ent[obj_id]

        # Degrees
        sub_degree = row['sub_degree'].item()
        obj_degree = get_degree(obj_name)

        # Graph data stuff
        right_path, left_path = get_path(sub_name, obj_name)
        if len(right_path) > 3:
            right_path = []
        if len(left_path) > 3:
            left_path = []

        # Setting the shortest distance
        if not right_path and not left_path:
            shortest_dist = 0
        elif right_path and not left_path:
            shortest_dist = len(right_path)
        elif left_path and not right_path:
            shortest_dist = len(left_path)
        else:
            shortest_dist = min(len(right_path), len(left_path))

        right_path = '->'.join(right_path)
        left_path = '<-'.join(left_path[::-1])

        # Score Stuff
        conf = round(conf.item(), 4)
        # avg_score = round(avg[obj_id].item(), 8)

        # if sub_id == 0 and rel_id == 0 and obj_id == 5540:
        # print(row.index.item(), row)

        table_rows.append(
            [df_ind, sub_id, rel_id, obj_id, sub_name, rel_name, obj_name, conf, avg_score, sub_degree, obj_degree,
             shortest_dist, right_path, left_path])

    # print(tabulate(table_rows, headers=headers))
    return table_rows


if __name__ == '__main__':

    # Load model
    args = {'name': 'testrun',
            'dataset': 'RLF/lffam-cp',
            'model': 'compgcn',
            'score_func': 'conve',
            'opn': 'corr',
            'use_wandb': False,
            'batch_size': 128,
            'gamma': 40.0,
            'gpu': 0,
            'max_epochs': 1,
            'l2': 0.0,
            'lr': 0.001,
            'lbl_smooth': 0.1,
            'num_workers': 10,
            'seed': 41504,
            'restore': False,
            'bias': False,
            'num_bases': -1,
            'init_dim': 100,
            'gcn_dim': 200,
            'embed_dim': None,
            'gcn_layer': 1,
            'dropout': 0.05,
            'hid_drop': 0.15,
            'hid_drop2': 0.15,
            'feat_drop': 0.15,
            'k_w': 10,
            'k_h': 20,
            'num_filt': 200,
            'ker_sz': 7,
            'log_dir': './log/',
            'config_dir': './config/',
            'trim': False,
            'trim_ratio': 0.00005,
            'use_fasttext': False
            }
    args = FancyDict(args)
    model = Runner(args)
    model.load_model('./checkpoints/rlf-lffam-cp')

    # Get graph
    pathdir = Path('./mc_dropout') / 'rlf-lffam-cp'

    triplets = pd.read_csv("data/RLF/lffam-cp/filter_triplets.txt", sep='\t')
    graph_nx = get_graph_nx(triplets)
    nodes_degree = [(node, graph_nx.degree[node]) for node in graph_nx.nodes]
    nodes_degree_df = pd.DataFrame(nodes_degree, columns=['sub_name', 'sub_degree'])

    graph_df = pd.read_pickle(pathdir / 'graph.pickle')
    graph_df['sub_name'] = graph_df['sub'].map(model.id2ent)
    graph_df['rel_name'] = graph_df['rel'].map(model.id2rel)
    graph_df = graph_df.merge(nodes_degree_df, how='left', on='sub_name')
    assert graph_df['sub_degree'].isna().sum() == 0, "There are some nodes with no degree"

    # Get predictions
    threshold_top_k = 10
    threshold_confidence = 0.5
    max_values_for_one_sub_rel = 10
    upto = 0
    predictions = []

    fnames = sorted([int(fname.stem) for fname in pathdir.rglob('*.torch')])

    for fname in tqdm(fnames):
        actual_fname = pathdir / f"{fname}.torch"
        preds = torch.load(actual_fname, map_location=torch.device('cpu'))
        preds_mcd = preds['mc_dropout']
        preds_van = preds['vanilla']


        for i in range(preds_mcd.shape[0]):
            row = graph_df.loc[upto + i]    #preds_mcd.shape[0] = 5 so upto+i always max 5

            # Take care of creating the mask and doing the softmax
            pred = preds_mcd[i]
            mask = torch.zeros(model.p.num_ent, dtype=torch.bool)
            mask[row['all_obj']] = True

            pred[:, mask] = -10 ** 10

            # Do softmax (per row) over the pred
            pred = torch.softmax(pred, dim=1)

            topk = top_k(pred, k=threshold_top_k)
            newedges = aggregate(pred, topk, model=model, row=row, conf_threshold=0.5,
                                 avg_threshold=0.00, max_items=10, df_ind=upto+i)
            if newedges:
                predictions.append(newedges)

        upto += preds_mcd.shape[0]

    flats = [y for x in predictions for y in x]
    #print(tabulate(flats, headers=['DF Ind', 'Sub ID', 'Rel ID', 'Obj ID', 'Sub Nm', 'Rel Nm', 'Obj Nm', 'Conf', 'Score', 'Sub Deg', Obj Deg', 'Short. Dist.', 'Right Path', 'Left Path']))
    df = pd.DataFrame(flats, columns=['DF Ind', 'Sub ID','Rel ID','Obj ID','Sub Nm','Rel Nm','Obj Nm','Conf','Score','Sub Deg','Obj Deg', 'Short. Dist.','Right Path', 'Left Path'])
    df.to_csv('predictions_final.csv', sep='\t')
